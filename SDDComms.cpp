#include "SDDComms.h"

/* Public methods: */
/* Only this methods are used in Serpent. */

/* Constructor: */
SDDComms::SDDComms() {};

/* Destructor: */
SDDComms::~SDDComms() {};

/* Allocate and initialize all the internal data structures: */
void SDDComms::initialize(int buff_size, 
   int min_asynch_matches_for_synch) {
   
   /* Get the MPI parameters: */
   initializeMPI();
   
   /* Create the MPI type used to send and recieve neutrons: */
   createNeutronMPIType();
   
   /* Initialize the (zero-based) cycle number: */
   num_cycle = 0;
   
   /* Set the limit size for the neutron buffers: */
   buffer_size = buff_size;
   
   /* Minimum number of asynch. balance matches to request synch.: */
   min_asynch_balance_matches = min_asynch_matches_for_synch;
   
   /* Minimum number of processes finished to synchronize: */
   /* Warning: setting this parameter to something other that 1 is */
   /*    likely to cause deadlocks. */
   min_synch_requests = 1;
   
   /* Allocate the local particle counters for all domains: */
   n0_local.resize(ntasks); // Number of particles created.
   nf_local.resize(ntasks); // Number of particles finished.
   
   /* Allocate the local asynch. balance matches for all domains: */
   asynch_balance_matches.resize(ntasks);
   
   /* Allocate the synchronization requests: */
   synchronization_requests.resize(ntasks);
   
   /* Indexes for buffers used to send neutrons: */
   index_send_buff_neutrons.resize(ntasks);
   
   /* Allocate the buffers for MPI send operations (MPI_Isend): */
   send_buff_neutrons.resize(ntasks); // Neutrons.
   send_buff_balance.resize(ntasks); // Balance.
   send_buff_balance_match.resize(ntasks); // Balance match.
   send_buff_synch_request.resize(ntasks); // Synch. requests.
   
   /* Allocate the buffers for MPI recieve operations (MPI_Irecv): */
   recv_buff_neutrons.resize(ntasks); // Neutr.
   recv_buff_balance.resize(ntasks); // Balance.
   recv_buff_balance_match.resize(ntasks); // Balance match.
   recv_buff_synch_request.resize(ntasks); // Synch. requests.
   
   /* Allocate the requests (MPI_Request) for send operations: */
   send_req_neutrons.resize(ntasks); // Neutrons.
   send_req_balance.resize(ntasks); // Balance.
   send_req_balance_match.resize(ntasks); // Balance match.
   send_req_synch_request.resize(ntasks); // Synch. req.
   
   /* Allocate the requests (MPI_Request) for recieve operations: */
   recv_req_neutrons.resize(ntasks); // Neutrons.
   recv_req_balance.resize(ntasks); // Balance.
   recv_req_balance_match.resize(ntasks); // Balance match.
   recv_req_synch_request.resize(ntasks); // Synch. req.
   
   /* Allocate the status (MPI_Status) for send operations: */
   send_stat_neutrons.resize(ntasks); // Neutrons.
   send_stat_balance.resize(ntasks); // Balance.
   send_stat_balance_match.resize(ntasks); // Balance match.
   send_stat_synch_request.resize(ntasks); // Synch. req.
   
   /* Allocate the status (MPI_Status) for recieve operations: */
   recv_stat_neutrons.resize(ntasks); // Neutrons.
   recv_stat_balance.resize(ntasks); // Balance.
   recv_stat_balance_match.resize(ntasks); // Balance match.
   recv_stat_synch_request.resize(ntasks); // Synch. req.
   
   /* Allocate the flags to manage waiting for sends: */
   wait_for_neutron_send.resize(ntasks); // Neutrons.
   wait_for_balance_send.resize(ntasks); // Balance.
   wait_for_balance_match_send.resize(ntasks); // Balance match.
   wait_for_synch_request_send.resize(ntasks); // Synch. req.
   
   /* Allocate the neutron and balance buffers: */
   for (int did = 0; did < ntasks; did++) {
      if (did != pid) {
         index_send_buff_neutrons[did] = 0;
         send_buff_neutrons[did] = new Neutron[buffer_size];
         recv_buff_neutrons[did].resize(buffer_size);
         send_buff_balance[did].resize(2);
         recv_buff_balance[did].resize(2);
      }
   }
   
   /* Reset the counters and flags: */
   reset();
   
};

/* Free the object: */
void SDDComms::free() {
   
   /* Free the MPI type used to send and recieve neutrons: */
   MPI_Type_free(&MPI_Neutron);
   
};

/* Post asynchronous recieves for all incoming messages: */
void SDDComms::postAllRecieves() {
   
   /* Post recieves for all messages for each external domain: */
   for (int did = 0; did < ntasks; did++) {
      if (did != pid) {
         postNeutronRecieve(did);
         postNeutronBalanceRecieve(did);
         postAsynchronousBalanceMatchRecieve(did);
         postSynchronizationRequestRecieve(did);
      }
   }
   
};

/* Send a neutron to another domain: */
void SDDComms::sendNeutron(Neutron neutron, int did) {
   
   /* Wait for the previous asynchronous send: */
   if (wait_for_neutron_send[did]) {
      MPI_Wait(&send_req_neutrons[did], &send_stat_neutrons[did]);
      index_send_buff_neutrons[did] = 0;
      wait_for_neutron_send[did] = 0;
   }
   
   /* Add the neutron to the send buffer: */
   send_buff_neutrons[did][index_send_buff_neutrons[did]] = neutron;
   index_send_buff_neutrons[did]++;
   
   /* Send the buffer: */
   if (index_send_buff_neutrons[did] == buffer_size)
      sendNeutronBuffer(did);
   
};

/* Flush all neutron buffers to other domains: */
void SDDComms::flushAllNeutronBuffers() {
   
   /* Send the neutron buffers to each external domain: */
   for (int did = 0; did < ntasks; did++) {
      if (did != pid) {
         
         /* Wait for the previous asynchronous send: */
         // BORRAR ESTO!! NO TIENE SENTIDO ESPERAR...
         if (wait_for_neutron_send[did]) {
            MPI_Wait(&send_req_neutrons[did], &send_stat_neutrons[did]);
            index_send_buff_neutrons[did] = 0;
            wait_for_neutron_send[did] = 0;
         }
         
         /* Send the buffer: */
         sendNeutronBuffer(did);
      }
   }
   
};

/* Get recieved neutrons from all other domains: */
std::vector<Neutron> SDDComms::getRecievedNeutrons() {
   
   /* Flag to check if a recieve operation has been completed: */
   int flag;
   
   /* New neutrons: */
   std::vector<Neutron> new_neutrons;
   
   /* New source: */
   std::vector<Neutron> new_source;
   
   /* Check asynchronous recieves from all other domains: */
   for (int did = 0; did < ntasks; did++) {
      if (did != pid) {
         
         /* Check if new neutrons have arrived from this domain: */
         MPI_Test(&recv_req_neutrons[did], &flag, 
            &recv_stat_neutrons[did]);
         
         /* Get the neutrons and post a new asynchronous recieve: */
         if (flag) {
            new_neutrons = processRecieveNeutronBuffer(did);
            new_source.insert(new_source.end(), new_neutrons.begin(), 
               new_neutrons.end());
            postNeutronRecieve(did);
            num_recvs_local[0]++;
         }
         
      }
   }
   
   /* Return the new neutrons: */
   return new_source;
   
};

/* Manage the termination of the particle-processing loop: */
int SDDComms::checkTermination(int n0, int nf) {
   
   /* Send the local neutron balance to all other domains: */
   sendNeutronBalance(n0, nf);
   
   /* Process the global asynchronous neutron balance: */
   processNeutronBalance();
   
   /* Let all other domains know if my asynch. balance matches: */
   sendAsynchronousBalanceMatch();
   
   /* Process the asynch. balance matches from all other domains: */
   processAsynchronousBalanceMatches();
   
   /* Let all other domains know if I'm requesting to synchronize: */
   sendSynchronizationRequest();
   
   /* Process the synchronization requests from all other domains: */
   processSynchronizationRequests();
   
   /* Check the global synchronous neutron balance: */
   if (synchronize) {
      checkSynchronousNeutronBalance(n0, nf);
      synchronize = 0;
   }
   
   /* Return the particle-processing loop termination flag: */
   return done;
   
};

/* Send the last control messages: */
void SDDComms::sendLastControlMessages() {
   
   /* Last neutron balance message: */
   int n0, nf;
   
   /* Send the last neutron balance message: */
   n0 = -1;
   nf = -1;
   sendNeutronBalance(n0, nf);
   
   /* Send the last asynchronous balance match message: */
   asynch_balance_match = -1;
   sendAsynchronousBalanceMatch();
   
   /* Send the last synchronization request message: */
   request_synchronization = -1;
   sendSynchronizationRequest();
   
};

/* Recieve the last control messages: */
void SDDComms::recieveLastControlMessages() {
   
   /* Switch: */
   int last_message_recieved;
   
   /* Process the neutron balance until all messages are recieved: */
   last_message_recieved = 0;
   while (!last_message_recieved) {
      processNeutronBalance();
      last_message_recieved = 1;
      for (int did = 0; did < ntasks; did++) {
         if(n0_local[did] != -1)
            last_message_recieved = 0;
         if(nf_local[did] != -1)
            last_message_recieved = 0;
      }
   }
   
   /* Process the balance matches until all messages are recieved: */
   last_message_recieved = 0;
   while (!last_message_recieved) {
      processAsynchronousBalanceMatches();
      last_message_recieved = 1;
      for (int did = 0; did < ntasks; did++) {
         if(asynch_balance_matches[did] != -1)
            last_message_recieved = 0;
      }
   }
   
   /* Process the synch. requests until all messages are recieved: */
   last_message_recieved = 0;
   while (!last_message_recieved) {
      processSynchronizationRequests();
      last_message_recieved = 1;
      for (int did = 0; did < ntasks; did++) {
         if(synchronization_requests[did] != -1)
            last_message_recieved = 0;
      }
   }
   
};

/* Cancel all asynchronous recieves for all other domains: */
void SDDComms::cancelAllRecieves() {
   
   /* Cancel recieves for all messages for each external domain: */
   for (int did = 0; did < ntasks; did++) {
      if (did != pid) {
         MPI_Cancel(&recv_req_neutrons[did]);
         MPI_Cancel(&recv_req_balance[did]);
         MPI_Cancel(&recv_req_balance_match[did]);
         MPI_Cancel(&recv_req_synch_request[did]);
      }
   }
   
   /* Update the cycle number for next cycle: */
   num_cycle++;
   
   /* Reset the counters and flags for next cycle: */
   reset();
   
};

/* Get global results: */
void SDDComms::getGlobalResults(double *rp_local, double *rp_global, 
   double *ra_local, double *ra_global, int *cf_local, int *cf_global, 
   int *ca_local, int *ca_global, int *c2n_local, int *c2n_global, 
   int *cs_local, int *cs_global) {
   
   /* Get global reaction rates: */
   MPI_Allreduce(rp_local, rp_global, 1, MPI_DOUBLE, MPI_SUM, 
      MPI_COMM_WORLD);
   MPI_Allreduce(ra_local, ra_global, 1, MPI_DOUBLE, MPI_SUM, 
      MPI_COMM_WORLD);
   
   /* Get global reaction counters: */
   MPI_Allreduce(cf_local, cf_global, 1, MPI_INT, MPI_SUM, 
      MPI_COMM_WORLD);
   MPI_Allreduce(ca_local, ca_global, 1, MPI_INT, MPI_SUM, 
      MPI_COMM_WORLD);
   MPI_Allreduce(c2n_local, c2n_global, 1, MPI_INT, MPI_SUM, 
      MPI_COMM_WORLD);
   MPI_Allreduce(cs_local, cs_global, 1, MPI_INT, MPI_SUM, 
      MPI_COMM_WORLD);
   
   /* Get global message counters: */
   MPI_Allreduce(num_sends_local, num_sends_global, 4, MPI_INT, 
      MPI_SUM, MPI_COMM_WORLD);
   MPI_Allreduce(num_recvs_local, num_recvs_global, 4, MPI_INT, 
      MPI_SUM, MPI_COMM_WORLD);
   
   /* Check that all neutron messages sent have been recieved: */
   if (num_sends_global[0] != num_recvs_global[0]) {
      std::cerr << "Error: neutron messages lost!\n";
      MPI_Abort(MPI_COMM_WORLD, 0);
   }
   
   /* Check that all balance messages sent have been recieved: */
   if (num_sends_global[1] != num_recvs_global[1]) {
      std::cerr << "Error: balance messages lost!\n";
      MPI_Abort(MPI_COMM_WORLD, 0);
   }
   
   /* Check that all balance match messages sent have been recieved: */
   if (num_sends_global[2] != num_recvs_global[2]) {
      std::cerr << "Error: asynchronous balance match messages lost!\n";
      MPI_Abort(MPI_COMM_WORLD, 0);
   }
   
   /* Check that all synch. request messages sent have been recieved: */
   if (num_sends_global[3] != num_recvs_global[3]) {
      std::cerr << "Error: synchronization request messages lost!\n";
      MPI_Abort(MPI_COMM_WORLD, 0);
   }
   
};

/* Private methods: */
/* This methods are used internally and are not available to Serpent. */

/* Get the MPI parameters: */
void SDDComms::initializeMPI() {
   
   /* Get number of tasks: */
   MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
   
   /* Get my rank: */
   MPI_Comm_rank(MPI_COMM_WORLD, &pid);
   
};

/* Create the MPI type used to send and recieve neutrons: */
void SDDComms::createNeutronMPIType() {
   
   /* Structure dimensions and data types: */
   int size = 9; // Number of members in the structure.
   int lengths[9]; // Lenghts of the members.
   MPI_Aint offsets[9]; // Offsets in the structure.
   MPI_Datatype types[9]; // MPI datatypes of the members.
   
   /* Set the structure lenghts and MPI types: */
   for (int i = 0; i < 9; i++) {
      lengths[i] = 1;
      types[i] = MPI_DOUBLE;
   }
   
   /* Set the offsets: */
   offsets[0] = offsetof(Neutron, x);
   offsets[1] = offsetof(Neutron, y);
   offsets[2] = offsetof(Neutron, z);
   offsets[3] = offsetof(Neutron, u);
   offsets[4] = offsetof(Neutron, v);
   offsets[5] = offsetof(Neutron, w);
   offsets[6] = offsetof(Neutron, E);
   offsets[7] = offsetof(Neutron, wgt);
   offsets[8] = offsetof(Neutron, t);
   
   /* Create the MPI type: */
   MPI_Type_create_struct(size, lengths, offsets, types, &MPI_Neutron);
   MPI_Type_commit(&MPI_Neutron);
   
};

/* Reset the counters and flags: */
void SDDComms::reset() {
   
   /* Reset the local message counters: */
   num_sends_local[0] = 0; // Neutron sends.
   num_sends_local[1] = 0; // Neutron balance sends.
   num_sends_local[2] = 0; // Asynchronous balance match sends.
   num_sends_local[3] = 0; // Synchronization request sends.
   num_recvs_local[0] = 0; // Neutron recieves.
   num_recvs_local[1] = 0; // Neutron balance recieves.
   num_recvs_local[2] = 0; // Asynchronous balance match recieves.
   num_recvs_local[3] = 0; // Synchronization request recieves.
   
   /* Reset the local particle counters for all domains: */
   for (int did = 0; did < ntasks; did++) {
      n0_local[did] = 0; // Number of particles created.
      nf_local[did] = 0; // Number of particles finished.
   }
   
   /* Reset the local asynch. balance matches for all domains: */
   asynch_balance_match = 0; // Does my asynchronous balance match?
   for (int did = 0; did < ntasks; did++)
      asynch_balance_matches[did] = 0; // Which balances match?
   
   /* Reset the synchronization requests: */
   request_synchronization = 0; // Do enough balances match?
   for (int did = 0; did < ntasks; did++)
      synchronization_requests[did] = 0; // Who else thinks so?
   synchronize = 0; // Should we synchronize and check the balance?
   
   /* Reset the particle-processing loop termination flag: */
   done = 0;
   
   /* Reset the flags to manage waiting for sends to reuse buffers: */
   for (int did = 0; did < ntasks; did++) {
      wait_for_neutron_send[did] = 0; // Neutrons.
      wait_for_balance_send[did] = 0; // Balance.
      wait_for_balance_match_send[did] = 0; // Balance match.
      wait_for_synch_request_send[did] = 0; // Synch. req.
   }
   
};

/* Send a neutron buffer to another domain: */
void SDDComms::sendNeutronBuffer(int did) {
   
   /* If the buffer is empty, return: */
   if (index_send_buff_neutrons[did] == 0)
      return;
   
   /* Send the buffer: */
   MPI_Isend(&send_buff_neutrons[did][0], 
      index_send_buff_neutrons[did], MPI_Neutron, did, 
      num_cycle*4*ntasks + 0*ntasks + pid, MPI_COMM_WORLD, 
      &send_req_neutrons[did]);
   wait_for_neutron_send[did] = 1;
   num_sends_local[0]++;
   
};

/* Send the local neutron balance to all other domains: */
void SDDComms::sendNeutronBalance(int n0, int nf) {
   
   /* If nothing has changed since the last send, return: */
   if (n0 == n0_local[pid] && nf == nf_local[pid])
      return;
   
   /* Send the local balance to all other domains: */
   for (int did = 0; did < ntasks; did++) {
      
      /* Send the neutron balance to an external domain: */
      if (did != pid) {
         
         /* Wait for the previous asynchronous send: */
         if (wait_for_balance_send[did]) {
            MPI_Wait(&send_req_balance[did], &send_stat_balance[did]);
            wait_for_balance_send[did] = 0;
         }
         
         /* Send the neutron balance: */
         send_buff_balance[did][0] = n0;
         send_buff_balance[did][1] = nf;
         MPI_Isend(&send_buff_balance[did].front(), 2, MPI_INT, did, 
            num_cycle*4*ntasks + 1*ntasks + pid, MPI_COMM_WORLD, 
            &send_req_balance[did]);
         wait_for_balance_send[did] = 1;
         num_sends_local[1]++;
         
      }
      
      /* Store the neutron balance directly: */
      else {
         
         /* Copy the neutron balance: */
         n0_local[pid] = n0;
         nf_local[pid] = nf;
         
      }
   }
   
};

/* Let all other domains know if my asynch. balance matches: */
void SDDComms::sendAsynchronousBalanceMatch() {
   
   /* If nothing has changed since the last send, return: */
   if (asynch_balance_match == asynch_balance_matches[pid])
      return;
   
   /* Send the asynchronous balance match to all other domains: */
   for (int did = 0; did < ntasks; did++) {
      
      /* Send the balance match to an external domain: */
      if (did != pid) {
         
         /* Wait for the previous asynchronous send: */
         if (wait_for_balance_match_send[did]) {
            MPI_Wait(&send_req_balance_match[did], 
               &send_stat_balance_match[did]);
            wait_for_balance_match_send[did] = 0;
         }
         
         /* Send the balance match: */
         send_buff_balance_match[did] = asynch_balance_match;
         MPI_Isend(&send_buff_balance_match[did], 1, MPI_INT, did, 
            num_cycle*4*ntasks + 2*ntasks + pid, MPI_COMM_WORLD, 
            &send_req_balance_match[did]);
         wait_for_balance_match_send[did] = 1;
         num_sends_local[2]++;
         
      }
      
      /* Store the balance match directly: */
      else
         
         /* Copy the balance match: */
         asynch_balance_matches[pid] = asynch_balance_match;
         
   }
   
};

/* Let all other domains know if I'm requesting to synchronize: */
void SDDComms::sendSynchronizationRequest() {
   
   /* If nothing has changed since the last send, return: */
   if (request_synchronization == synchronization_requests[pid])
      return;
   
   /* Send the synchronization request to all other domains: */
   for (int did = 0; did < ntasks; did++) {
      
      /* Send the synchronization request to an external domain: */
      if (did != pid) {
         
         /* Wait for the previous asynchronous send: */
         if (wait_for_synch_request_send[did]) {
            MPI_Wait(&send_req_synch_request[did], 
               &send_stat_synch_request[did]);
            wait_for_synch_request_send[did] = 0;
         }
         
         /* Send the synchronization request: */
         send_buff_synch_request[did] = request_synchronization;
         MPI_Isend(&send_buff_synch_request[did], 1, MPI_INT, did, 
            num_cycle*4*ntasks + 3*ntasks + pid, MPI_COMM_WORLD, 
            &send_req_synch_request[did]);
         wait_for_synch_request_send[did] = 1;
         num_sends_local[3]++;
         
      }
      
      /* Store the synchronization request directly: */
      else
         
         /* Copy the synchronization request: */
         synchronization_requests[pid] = request_synchronization;
         
   }
   
};

/* Post an asynchronous recieve for neutrons: */
void SDDComms::postNeutronRecieve(int did) {
   
   /* Post a recieve for the maximum buffer size: */
   MPI_Irecv(&recv_buff_neutrons[did].front(), buffer_size, 
      MPI_Neutron, did, num_cycle*4*ntasks + 0*ntasks + did, 
      MPI_COMM_WORLD, &recv_req_neutrons[did]);
   
};

/* Post an asynchronous recieve for a local neutron balance: */
void SDDComms::postNeutronBalanceRecieve(int did) {
   
   /* Post a recieve for the created and finished histories: */
   MPI_Irecv(&recv_buff_balance[did].front(), 2, MPI_INT, did, 
      num_cycle*4*ntasks + 1*ntasks + did, MPI_COMM_WORLD, 
      &recv_req_balance[did]);
   
};

/* Post an asynchronous recieve for an asynch. balance match: */
void SDDComms::postAsynchronousBalanceMatchRecieve(int did) {
   
   /* Post a recieve for a balance match: */
   MPI_Irecv(&recv_buff_balance_match[did], 1, MPI_INT, did, 
      num_cycle*4*ntasks + 2*ntasks + did, MPI_COMM_WORLD, 
      &recv_req_balance_match[did]);
   
};

/* Post an asynchronous recieve for a synchronization request: */
void SDDComms::postSynchronizationRequestRecieve(int did) {
   
   /* Post a recieve for a synchronization request: */
   MPI_Irecv(&recv_buff_synch_request[did], 1, MPI_INT, did, 
      num_cycle*4*ntasks + 3*ntasks + did, MPI_COMM_WORLD, 
      &recv_req_synch_request[did]);
   
};

/* Process a recieve neutron buffer: */
std::vector<Neutron> SDDComms::processRecieveNeutronBuffer(int did) {
   
   /* Number of neutrons in the recieved message: */
   int size;
   
   /* New neutrons: */
   std::vector<Neutron> new_neutrons;
   
   /* Get the number of new neutrons: */
   MPI_Get_count(&recv_stat_neutrons[did], MPI_Neutron, &size);
   
   /* Resize the buffer: */
   recv_buff_neutrons[did].resize(size);
   
   /* Add the neutrons to the source: */
   for (int i = 0; i < size; i++) {
      new_neutrons.push_back(recv_buff_neutrons[did].back());
      recv_buff_neutrons[did].pop_back();
   }
   
   /* Restore the buffer: */
   recv_buff_neutrons[did].resize(buffer_size);
   
   /* Return the new neutrons: */
   return new_neutrons;
   
};

/* Process the global asynchronous neutron balance: */
void SDDComms::processNeutronBalance() {
   
   /* Flag to check if a recieve operation has been completed: */
   int flag;
   
   /* Global particle counters: */
   int n0_global; // Number of particles created.
   int nf_global; // Number of particles finished.
   
   /* Check asynchronous recieves from all other domains: */
   for (int did = 0; did < ntasks; did++) {
      if (did != pid) {
         
         /* Check if a new balance has arrived from this domain: */
         MPI_Test(&recv_req_balance[did], &flag, 
            &recv_stat_balance[did]);
         
         /* Get the balance and post a new asynchronous recieve: */
         if (flag) {
            n0_local[did] = recv_buff_balance[did][0];
            nf_local[did] = recv_buff_balance[did][1];
            postNeutronBalanceRecieve(did);
            num_recvs_local[1]++;
         }
         
      }
   }
   
   /* Initialize the global particle counters: */
   n0_global = 0; // Number of particles created.
   nf_global = 0; // Number of particles finished.
   
   /* Calculate the global particle counters: */
   for (int did = 0; did < ntasks; did++) {
      if(n0_local[did] > 0) {
         n0_global += n0_local[did];
         nf_global += nf_local[did];
      }
      else
         asynch_balance_match = 0;
   }
   
   /* Switch flag on the asynchronous balance matches: */
   if (n0_global == nf_global)
      asynch_balance_match = 1;
   else
      asynch_balance_match = 0;
   
};

/* Process the asynch. balance matches from all other domains: */
void SDDComms::processAsynchronousBalanceMatches() {
   
   /* Flag to check if a recieve operation has been completed: */
   int flag;
   
   /* Number of processes that have detected balance matches: */
   int num_asynch_balance_matches;
   
   /* Check asynchronous recieves from all other domains: */
   for (int did = 0; did < ntasks; did++) {
      if (did != pid) {
         
         /* Check if a new match has arrived from this domain: */
         MPI_Test(&recv_req_balance_match[did], &flag, 
            &recv_stat_balance_match[did]);
         
         /* Get the match and post a new asynchronous recieve: */
         if (flag) {
            asynch_balance_matches[did] = recv_buff_balance_match[did];
            postAsynchronousBalanceMatchRecieve(did);
            num_recvs_local[2]++;
         }
         
      }
   }
   
   /* Get the number of processes that have detected balance matches: */
   num_asynch_balance_matches = 0;
   for (int did = 0; did < ntasks; did++) {
      if(asynch_balance_matches[did] == 1)
         num_asynch_balance_matches++;
   }
   
   /* Check if there're enough matches to think we're done: */
   if (num_asynch_balance_matches >= min_asynch_balance_matches)
      request_synchronization = 1;
   else
      request_synchronization = 0;
   
};

/* Process the synchronization requests from all other domains: */
void SDDComms::processSynchronizationRequests() {
   
   /* Flag to check if a recieve operation has been completed: */
   int flag;
   
   /* Number of processes that want to synchronize: */
   int num_synch_matches;
   
   /* Check asynchronous recieves from all other domains: */
   for (int did = 0; did < ntasks; did++) {
      if (did != pid) {
         
         /* Check if a new request has arrived from this domain: */
         MPI_Test(&recv_req_synch_request[did], &flag, 
            &recv_stat_synch_request[did]);
         
         /* Get the request and post a new asynchronous recieve: */
         if (flag) {
            synchronization_requests[did] = 
               recv_buff_synch_request[did];
            postSynchronizationRequestRecieve(did);
            num_recvs_local[3]++;
         }
         
      }
   }
   
   /* Get the number of processes that want to synchronize: */
   num_synch_matches = 0;
   for (int did = 0; did < ntasks; did++) {
      if(synchronization_requests[did] == 1)
         num_synch_matches++;
   }
   
   /* Check if enough processes want to synchronize: */
   if (num_synch_matches >= min_synch_requests)
      synchronize = 1;
   else
      synchronize = 0;
   
};

/* Check the global synchronous neutron balance: */
void SDDComms::checkSynchronousNeutronBalance(int n0, int nf) {
   
   /* Local balance: */
   int n_local[2] = {n0, nf};
   
   /* Global balance: */
   int n_global[2] = {0, 0};
   
   /* Get global neutron balance: */
   MPI_Allreduce(n_local, n_global, 2, MPI_INT, MPI_SUM, 
      MPI_COMM_WORLD);
   
   /* Check the neutron balance: */
   if (n_global[0] == n_global[1])
      done = 1;
   
};
