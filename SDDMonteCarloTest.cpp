#include "SDDMonteCarloTest.h"

/* Constructor: */
SDDMonteCarloTest::SDDMonteCarloTest() {};

/* Destructor: */
SDDMonteCarloTest::~SDDMonteCarloTest() {};

/* Start execution: */
void SDDMonteCarloTest::start() {
   
   /* Get number of tasks: */
   MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
   
   /* Get my rank: */
   MPI_Comm_rank(MPI_COMM_WORLD, &pid);
   
   /* Buffer size: */
   int buff_size = 100;
   int min_asynch_matches_for_synch = ntasks;
   
   /* Create and initialize the SDDComms object: */
   comms = SDDComms();
   comms.initialize(buff_size, min_asynch_matches_for_synch);
   
   /* Initialize the output flag: */
   output_enabled = 1;
   
   /* Initialize the flag for runs checking for race conditions: */
   check_race_conditions = 0;
   
   /* Initialize the (zero-based) cycle number: */
   num_cycle = -1;
   
   /* Initialize the system dimensions: */
   x0 = 0.0; // x0 of the system.
   xf = 1.0; // xf of the system.
   
   /* Initialize the dimensions of this domain: */
   dx = (xf-x0) / ntasks; // dx of this domain.
   x1 = pid * dx; // x1 limit of this domain.
   x2 = (pid+1) * dx; // x2 limit of this domain.
   
   /* Initialize the system probabilities: */
   pf = 0.3; // Fission probability.
   pa = 0.85; // Absortion probability.
   p2n = 0.9; // (n, 2n) probability.
   ps = 1.0; // Scattering probability.
   nu = 2.2; // Average number of neutrons per fission;
   
   /* Initialize the local reaction rates: */
   rp_local = 1.0 / ntasks; // Production.
   ra_local = 0.0; // Absorption.
   
   /* Initialize the random number generator: */
   //srand((unsigned)time(NULL)+pid*ntasks);
   srand(pid*ntasks);
   
};

/* Finish execution: */
void SDDMonteCarloTest::finish() {
   
   /* Free the communications: */
   comms.free();
   
};

/* Check for race conditions: */
void SDDMonteCarloTest::check(int num_runs, int num_cycles, int num_neutrons) {
   
   /* Initialize the reaction counters: */
   std::vector<int> cf_global0; // Fission.
   std::vector<int> ca_global0; // Absorption.
   std::vector<int> c2n_global0; // (n, 2n).
   std::vector<int> cs_global0; // Scattering.
   
   /* Allocate the reaction counters: */
   cf_global0.reserve(num_cycles); // Fission.
   ca_global0.reserve(num_cycles); // Absorption.
   c2n_global0.reserve(num_cycles); // (n, 2n).
   cs_global0.reserve(num_cycles); // Scattering.
   
   /* Run multiple full calculations: */
   for (int i = 0; i < num_runs; i++) {
      
      /* Start (reset all the class variables): */
      start();
      
      /* Disable output: */
      output_enabled = 0;
      
      /* Switch on the flag for runs checking for race conditions: */
      check_race_conditions = 1;
      
      /* Print the run number: */
      if (pid == 0)
         std::cout << "Run " << i << "...\n";
      
      /* Run all the cycles: */
      for (int j = 0; j < num_cycles; j++) {
         
         /* Run one cycle: */
         calculate(num_neutrons);
         
         /* Get global results: */
         comms.getGlobalResults(&rp_local, &rp_global, &ra_local, 
            &ra_global, &cf_local, &cf_global, &ca_local, &ca_global, 
            &c2n_local, &c2n_global, &cs_local, &cs_global);
         
         /* Check for race conditions: */
         if (i > 0) {
            if (cf_global != cf_global0[j] && pid == 0) {
               std::cerr << "Race condition detected in fission!\n";
               MPI_Abort(MPI_COMM_WORLD, 0);
            }
            if (ca_global != ca_global0[j] && pid == 0) {
               std::cerr << "Race condition detected in absorption!\n";
               MPI_Abort(MPI_COMM_WORLD, 0);
            }
            if (c2n_global != c2n_global0[j] && pid == 0) {
               std::cerr << "Race condition detected in (n, 2n)!\n";
               MPI_Abort(MPI_COMM_WORLD, 0);
            }
            if (cs_global != cs_global0[j] && pid == 0) {
               std::cerr << "Race condition detected in scattering!\n";
               MPI_Abort(MPI_COMM_WORLD, 0);
            }
         }
         
         /* Update values: */
         cf_global0[j] = cf_global;
         ca_global0[j] = ca_global;
         c2n_global0[j] = c2n_global;
         cs_global0[j] = cs_global;
         
      }
      
      /* Enable output: */
      output_enabled = 1;
      
      /* Switch off the flag for runs checking for race conditions: */
      check_race_conditions = 0;
      
      /* Finish: */
      finish();
      
   }
   
};

/* Calculate: */
void SDDMonteCarloTest::calculate(int N) {
   
   /* Neutron variables: */
   double p; // Random number.
   Neutron neutron; // Neutron object.
   
   /* Multiplication factors: */
   double k; // Calculated multiplication factor.
   double k0; // Theoretical multiplication factor.
   
   /* Update the cycle number: */
   num_cycle++;
   
   /* Initialize the neutron source: */
   initializeSource(N);
   
   /* Initialize the reaction rates: */
   rp_local = 0.0; // Production.
   ra_local = 0.0; // Absorption.
   
   /* Initialize the reaction counters: */
   cf_local = 0; // Fission.
   ca_local = 0; // Absorption.
   c2n_local = 0; // (n, 2n).
   cs_local = 0; // Scattering.
   
   /* Initialize the termination control flag: */
   done = 0;
   
   /* Post asynchronous recieves for all other domains: */
   comms.postAllRecieves();
   
   /* Run all the neutrons: */
   while (!done) {
      
      /* If neutrons are available run a history: */
      if (!source.empty()) {
         
         /* Get a neutron: */
         neutron = source.back();
         source.pop_back();
         
         /* Get the reaction: */
         p = getRandomNumber();
         if (p < pf)
            fissionReaction(neutron);
         else if (p < pa)
            absorptionReaction(neutron);
         else if (p < p2n)
            n2nReaction(neutron);
         else
		      scatteringReaction(neutron);
         
      }
      
      /* If no neutrons are available manage the communications: */
      else {
         
         /* Send all half-full buffers: */
         comms.flushAllNeutronBuffers();
         
         /* Process the asynchronous recieves: */
         source = comms.getRecievedNeutrons();
         
         /* If no new neutrons have arrived, check convergence: */
         if (source.empty())
            done = comms.checkTermination(n0, nf);
         
      }
      
   }
   
   /* Send the last asynchronous and synchronous control messages: */
   comms.sendLastControlMessages();
   
   /* Get the last asynchronous and synchronous control messages: */
   comms.recieveLastControlMessages();
   
   /* Get global results: */
   comms.getGlobalResults(&rp_local, &rp_global, &ra_local, &ra_global, 
      &cf_local, &cf_global, &ca_local, &ca_global, &c2n_local, 
      &c2n_global, &cs_local, &cs_global);
   
   /* Normalize the local production rate: */
   rp_local = rp_local / rp_global;
   
   /* Calculate numerical and theoretical multiplication factors: */
   k = rp_global / ra_global;
   k0 = nu*pf / (pa-pf);
   
   /* Cancel asynchronous recieves for all other domains: */
   comms.cancelAllRecieves();
   
};

/* Get a random double in (0.0, 1.0): */
double SDDMonteCarloTest::getRandomNumber() {
   
   /* Return a double in (0.0, 1.0): */
   return (double)rand()/RAND_MAX;
   
};

/* Initialize the neutron source in this domain: */
void SDDMonteCarloTest::initializeSource(int N) {
   
   /* Reset the local reaction rates: */
   if (check_race_conditions) {
      rp_local = 1.0 / ntasks; // Production.
      ra_local = 0.0; // Absorption.
   }
   
   /* Internal variables: */
   int n; // Number of source neutrons in this domain.
   Neutron neutron; // Neutron object.
   
   /* Get the number of neutrons in this domain: */
   n = (int)(rp_local*N) + 1;
   
   /* Create the neutrons and add them to the source: */
   for (int i = 0; i < n; i++) {
      neutron = Neutron();
      neutron.x = x1 + dx*getRandomNumber();
      neutron.y = 0.0;
      neutron.z = 0.0;
      neutron.u = 0.0;
      neutron.v = 0.0;
      neutron.w = 0.0;
      neutron.E = 0.0;
      neutron.t = 0.0;
      if (i != n-1)
         neutron.wgt = 1.0;
      else
         neutron.wgt = rp_local*N - n + 1;
      source.push_back(neutron);
   }
   
   /* Number of particles created: */
   n0 = n;
   
   /* Number of particles finished: */
   nf = 0;
   
};

/* Fission reaction: */
void SDDMonteCarloTest::fissionReaction(Neutron neutron) {
   
   /* Add the reaction to the production rate: */
   rp_local += neutron.wgt * nu;
   
   /* Count the fission reaction: */
   cf_local++;
   
   /* Count the finish histories: */
   nf++;
   
};

/* Absorption reaction: */
void SDDMonteCarloTest::absorptionReaction(Neutron neutron) {
   
   /* Add the reaction to the absorption rate: */
   ra_local += neutron.wgt;
   
   /* Count the absorption reaction: */
   ca_local++;
   
   /* Count the finish histories: */
   nf++;
   
};

/* (n, 2n) reaction: */
void SDDMonteCarloTest::n2nReaction(Neutron neutron) {
   
   /* Create a new neutron: */
   Neutron neutron2 = neutron;
   
   /* Add the new neutron to the source: */
   source.push_back(neutron2);
   
   /* Return the original neutron to the source: */
   source.push_back(neutron);
   
   /* Count the (n, 2n) reaction: */
   c2n_local++;
   
   /* Count the started histories: */
   n0++;
   
};

/* Scattering reaction: */
void SDDMonteCarloTest::scatteringReaction(Neutron neutron) {
   
   /* New position: */
   double x; // Neutron position.
   int did; // New domain.
   
   /* Calculate the neutron position and domain: */
   x = (xf-x0) * getRandomNumber();
   did = (int)(x/dx);
   
   /* Change the neutron position: */
   neutron.x = x;
   
   /* Add the neutron to the source or send it to another domain: */
   if (did == pid)
      source.push_back(neutron);
   else
      comms.sendNeutron(neutron, did);
   
   /* Count the (n, 2n) reaction: */
   cs_local++;
   
};
