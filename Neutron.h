#ifndef NEUTRON_H
#define NEUTRON_H 

/* The Neutron struct: */
typedef struct Neutron {
   
   /* Position: */
   double x; // x-coordinate.
   double y; // y-coordinate.
   double z; // z-coordinate.
   
   /* Direction cosines: */
   double u; // x-direction.
   double v; // y-direction.
   double w; // z-direction.
   
   /* Energy: */
   double E;
   
   /* Weight: */
   double wgt;
   
   /* Time: */
   double t;
   
} Neutron;

#endif
