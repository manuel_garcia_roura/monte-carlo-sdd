#ifndef SDDCOMMS_H
#define SDDCOMMS_H

#include "Neutron.h"

#include "mpi.h"

#include <vector>
#include <iostream>
#include <stddef.h>

/* The SDDComms class: */
class SDDComms {
   
   private:
      
      /* MPI parameters: */
      int ntasks; // Number of MPI tasks.
      int pid; // MPI rank of the current process.
      
      /* MPI type used to send and recieve neutrons: */
      MPI_Datatype MPI_Neutron;
      
      /* Cycle number: */
      int num_cycle;
      
      /* Limit size for the neutron buffers: */
      int buffer_size;
      
      /* Local message counters: */
      /* Indexes: */
      /*    0 = Neutrons. */
      /*    1 = Neutron balances. */
      /*    2 = Asynchronous balance matches. */
      /*    3 = Synchronization requests. */
      int num_sends_local[4]; // Sends.
      int num_recvs_local[4]; // Recieves.
      
      /* Global message counters: */
      /* Indexes: */
      /*    0 = Neutrons. */
      /*    1 = Neutron balances. */
      /*    2 = Asynchronous balance matches. */
      /*    3 = Synchronization requests. */
      int num_sends_global[4]; // Sends.
      int num_recvs_global[4]; // Recieves.
      
      /* Minimum number of asynch. balance matches to request synch.: */
      int min_asynch_balance_matches;
      
      /* Minimum number of processes finished to synchronize: */
      /* Warning: setting this parameter to something other that 1 is */
      /*    likely to cause deadlocks. */
      int min_synch_requests;
      
      /* Local particle counters for all domains: */
      std::vector<int> n0_local; // Number of particles created.
      std::vector<int> nf_local; // Number of particles finished.
      
      /* Local asynch. balance matches for all domains: */
      int asynch_balance_match; // Does my asynchronous balance match?
      std::vector<int> asynch_balance_matches; // Which balances match?
      
      /* Synchronization requests: */
      int request_synchronization; // Do enough balances match?
      std::vector<int> synchronization_requests; // Who else thinks so?
      int synchronize; // Should we synchronize and check the balance?
      
      /* Particle-processing loop termination flag: */
      int done;
      
      /* Indexes for buffers used to send neutrons: */
      std::vector<int> index_send_buff_neutrons;
      
      /* Buffers for MPI send operations (MPI_Isend): */
      std::vector<Neutron*> send_buff_neutrons; // Neutrons.
      std::vector< std::vector<int> > send_buff_balance; // Balance.
      std::vector<int> send_buff_balance_match; // Balance match.
      std::vector<int> send_buff_synch_request; // Synch. requests.
      
      /* Buffers for MPI recieve operations (MPI_Irecv): */
      std::vector< std::vector<Neutron> > recv_buff_neutrons; // Neutr.
      std::vector< std::vector<int> > recv_buff_balance; // Balance.
      std::vector<int> recv_buff_balance_match; // Balance match.
      std::vector<int> recv_buff_synch_request; // Synch. requests.
      
      /* Requests (MPI_Request) for send operations: */
      std::vector<MPI_Request> send_req_neutrons; // Neutrons.
      std::vector<MPI_Request> send_req_balance; // Balance.
      std::vector<MPI_Request> send_req_balance_match; // Balance match.
      std::vector<MPI_Request> send_req_synch_request; // Synch. req.
      
      /* Requests (MPI_Request) for recieve operations: */
      std::vector<MPI_Request> recv_req_neutrons; // Neutrons.
      std::vector<MPI_Request> recv_req_balance; // Balance.
      std::vector<MPI_Request> recv_req_balance_match; // Balance match.
      std::vector<MPI_Request> recv_req_synch_request; // Synch. req.
      
      /* Status (MPI_Status) for send operations: */
      std::vector<MPI_Status> send_stat_neutrons; // Neutrons.
      std::vector<MPI_Status> send_stat_balance; // Balance.
      std::vector<MPI_Status> send_stat_balance_match; // Balance match.
      std::vector<MPI_Status> send_stat_synch_request; // Synch. req.
      
      /* Status (MPI_Status) for recieve operations: */
      std::vector<MPI_Status> recv_stat_neutrons; // Neutrons.
      std::vector<MPI_Status> recv_stat_balance; // Balance.
      std::vector<MPI_Status> recv_stat_balance_match; // Balance match.
      std::vector<MPI_Status> recv_stat_synch_request; // Synch. req.
      
      /* Flags to manage waiting for sends to reuse buffers: */
      std::vector<int> wait_for_neutron_send; // Neutrons.
      std::vector<int> wait_for_balance_send; // Balance.
      std::vector<int> wait_for_balance_match_send; // Balance match.
      std::vector<int> wait_for_synch_request_send; // Synch. req.
      
      /* Get the MPI parameters: */
      void initializeMPI();
      
      /* Create the MPI type used to send and recieve neutrons: */
      void createNeutronMPIType();
      
      /* Reset the counters and flags: */
      void reset();
      
      /* Send a neutron buffer to another domain: */
      void sendNeutronBuffer(int did);
      
      /* Send the local neutron balance to all other domains: */
      void sendNeutronBalance(int n0, int nf);
      
      /* Let all other domains know if my asynch. balance matches: */
      void sendAsynchronousBalanceMatch();
      
      /* Let all other domains know if I'm requesting to synchronize: */
      void sendSynchronizationRequest();
      
      /* Post an asynchronous recieve for neutrons: */
      void postNeutronRecieve(int did);
      
      /* Post an asynchronous recieve for a local neutron balance: */
      void postNeutronBalanceRecieve(int did);
      
      /* Post an asynchronous recieve for an asynch. balance match: */
      void postAsynchronousBalanceMatchRecieve(int did);
      
      /* Post an asynchronous recieve for a synchronization request: */
      void postSynchronizationRequestRecieve(int did);
      
      /* Process a recieve neutron buffer: */
      std::vector<Neutron> processRecieveNeutronBuffer(int did);
      
      /* Process the global asynchronous neutron balance: */
      void processNeutronBalance();
      
      /* Process the asynch. balance matches from all other domains: */
      void processAsynchronousBalanceMatches();
      
      /* Process the synchronization requests from all other domains: */
      void processSynchronizationRequests();
      
      /* Check the global synchronous neutron balance: */
      void checkSynchronousNeutronBalance(int n0, int nf);
   
   public:
      
      /* Constructor: */
      SDDComms();
      
      /* Destructor: */
      ~SDDComms();
      
      /* Allocate and initialize all the internal data structures: */
      void initialize(int buff_size, int min_asynch_matches_for_synch);
      
      /* Free the object: */
      void free();
      
      /* Post asynchronous recieves for all incoming messages: */
      void postAllRecieves();
      
      /* Send a neutron to another domain: */
      void sendNeutron(Neutron neutron, int did);
      
      /* Flush all neutron buffers to other domains: */
      void flushAllNeutronBuffers();
      
      /* Get recieved neutrons from all other domains: */
      std::vector<Neutron> getRecievedNeutrons();
      
      /* Manage the termination of the particle-processing loop: */
      int checkTermination(int n0, int nf);
      
      /* Send the last control messages: */
      void sendLastControlMessages();
      
      /* Recieve the last control messages: */
      void recieveLastControlMessages();
      
      /* Cancel all asynchronous recieves for all other domains: */
      void cancelAllRecieves(); 
      
      /* Get global results: */
      void getGlobalResults(double *rp_local, double *rp_global, 
         double *ra_local, double *ra_global, int *cf_local, 
         int *cf_global, int *ca_local, int *ca_global, int *c2n_local, 
         int *c2n_global, int *cs_local, int *cs_global);
   
};

#endif
