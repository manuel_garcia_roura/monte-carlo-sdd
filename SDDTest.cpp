#include "SDDMonteCarloTest.h"

#include "mpi.h"

int main() {
   
   /* Initialize MPI: */
   MPI_Init(NULL, NULL);
   
   /* Run type: */
   int check = 1;
   
   /* Instantiate a SDDMonteCarloTest solver: */
   SDDMonteCarloTest solver = SDDMonteCarloTest();
   
   /* Check for race conditions: */
   if (check) {
      int num_runs = 100;
      int num_cycles = 5;
      int num_neutrons = 1e5;
      solver.check(num_runs, num_cycles, num_neutrons);
   }
   
   /* Run a regular simulation: */
   else {
      int num_cycles = 1;
      int num_neutrons = 20;
      solver.start();
      for (int i = 0; i < num_cycles; i++)
         solver.calculate(num_neutrons);
      solver.finish();
   }
   
   /* Finalize MPI: */  
   MPI_Finalize();
   
};
