#ifndef SDDMONTECARLOTEST_H
#define SDDMONTECARLOTEST_H

#include "Neutron.h"
#include "SDDComms.h"

#include "mpi.h"

#include <vector>
#include <iostream>
#include <stdlib.h>

/* The SDDMonteCarloTest class: */
class SDDMonteCarloTest {
   
   private:
      
      /* MPI parameters: */
      int ntasks; // Number of MPI tasks.
      int pid; // MPI rank of the current process.
      
      /* SDDComms object: */
      SDDComms comms;
      
      /* Output flag: */
      int output_enabled;
      
      /* Flag for runs checking for race conditions: */
      int check_race_conditions;
      
      /* Cycle number: */
      int num_cycle;
      
      /* System dimensions: */
      double x0; // x0 of the system.
      double xf; // xf of the system.
      
      /* Domain decomposition dimensions: */
      double dx; // dx of this domain.
      double x1; // x1 limit of this domain.
      double x2; // x2 limit of this domain.
      
      /* System probabilities: */
      double pf; // Fission probability.
      double pa; // Absortion probability.
      double p2n; // (n, 2n) probability.
      double ps; // Scattering probability.
      double nu; // Average number of neutrons per fission;
      
      /* Local reaction rates: */
      double rp_local; // Production.
      double ra_local; // Absorption.
      
      /* Local reaction counters: */
      int cf_local; // Fission.
      int ca_local; // Absorption.
      int c2n_local; // (n, 2n).
      int cs_local; // Scattering.
      
      /* Local particle counters: */
      int n0; // Number of particles created in this domain.
      int nf; // Number of particles finished in this domain.
      
      /* Global reaction rates: */
      double rp_global; // Production.
      double ra_global; // Absorption.
      
      /* Global reaction counters: */
      int cf_global; // Fission.
      int ca_global; // Absorption.
      int c2n_global; // (n, 2n).
      int cs_global; // Scattering.
      
      /* Termination control flag: */
      int done;
      
      /* Neutron source: */
      std::vector<Neutron> source;
      
      /* Get a random double in (0.0, 1.0): */
      double getRandomNumber();
      
      /* Initialize the neutron source in this domain: */
      void initializeSource(int N);
      
      /* Fission reaction: */
      void fissionReaction(Neutron neutron);
      
      /* Absorption reaction: */
      void absorptionReaction(Neutron neutron);
      
      /* (n, 2n) reaction: */
      void n2nReaction(Neutron neutron);
      
      /* Scattering reaction: */
      void scatteringReaction(Neutron neutron);
   
   public:
      
      /* Constructor: */
      SDDMonteCarloTest();
      
      /* Destructor: */
      ~SDDMonteCarloTest();
      
      /* Start execution: */
      void start();
      
      /* Finish execution: */
      void finish();
      
      /* Check for race conditions: */
      void check(int num_runs, int num_cycles, int num_neutrons);
      
      /* Calculate: */
      void calculate(int N);
   
};

#endif
